# encoding: utf-8

# Process.setrlimit(:AS, 256000000)

require 'RMagick'

# Параметры, которые будут проверяться: разрешение (dpi) и качество, в котором сохранен файл
@x_res_min = 300
@y_res_min = 300
@quality_min = 100
# Сканируемая директория
@scan_dir = "/media/zv0r/Data/Partarhiv/"
# Лог со списком плохих файлов
# bad_files_log = "./bad_files_cdni.log"
# $stdout = File.open(bad_files_log, "w")
# Лог ошибок
# err_log = "./error_cdni.log"
# $stderr = File.open(err_log, "w")

def dir_processing(dir)
  a = Dir.new(dir)
  a.entries.each do |el|
    next if el =~ /^\./
    file = File.join(a.path, el)
    if File.directory?(file)
      dir_processing(file)
    else
      begin
        image = Magick::Image::read(file).first
        tmp_x_res = image.x_resolution.to_i
        tmp_y_res = image.y_resolution.to_i
        tmp_quality = image.quality
        unless tmp_x_res >= @x_res_min and tmp_y_res >= @y_res_min and tmp_quality >= @quality_min
          # Если файл с изображением не удовлетворяет нашим требованиям, запишем его в лог
          puts "Плохое качество (x_res: #{tmp_x_res}, y_res: #{tmp_y_res}, quality: #{tmp_quality}): #{file}"
          @bad_quality_files += 1
        end
      rescue
        # Если файл нечитаемый, запишем в лог
        puts "Нечитаемый файл: #{file}"
        @not_openable_files += 1
      end
    end
  end
end

@bad_quality_files = 0
@not_openable_files = 0
dir_processing(@scan_dir)
puts "\nИтого. Файлов в плохом качестве: #{bad_quality_files}, поврежденных файлов: #{not_openable_files}"